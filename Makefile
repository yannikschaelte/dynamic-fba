.PHONY: build run qa

################################################################################
# VARIABLES                                                                    #
################################################################################

GLPK_VERSION ?= 4.65
SUNDIALS_VERSION ?= 5.0.0
PLATFORM = "$(shell uname -s)"
ifeq ($(PLATFORM), "Darwin")  # Mac OS X
	NPROC ?= $(shell sysctl -n hw.physicalcpu)
else ifeq ($(PLATFORM), "Linux")  # Assume `nproc` command exists
	NPROC ?= $(shell expr $(shell nproc) - 1)
else
	@echo "Unsupported platform. You can set the environment variable NPROC and proceed at your own risk."
	NPROC ?= 1
endif

################################################################################
# COMMANDS                                                                     #
################################################################################

## Build the Docker image.
build-base:
	docker pull ubuntu:19.10
	docker build \
		--build-arg NPROC=$(NPROC) \
		--build-arg GLPK_VERSION=$(GLPK_VERSION) \
		--build-arg SUNDIALS_VERSION=$(SUNDIALS_VERSION) \
		--tag davidtourigny/dfba-base:3.7-$(GLPK_VERSION)-$(SUNDIALS_VERSION) \
		--file Dockerfile.base .

## Build the Docker image.
build: build-base
	docker build \
		--tag davidtourigny/dfba:3.7-$(GLPK_VERSION)-$(SUNDIALS_VERSION) \
		--tag davidtourigny/dfba:latest .

## Build the interactive Docker image.
build-interactive: build-base
	docker build --file Dockerfile.interactive --tag davidtourigny/dfba:interactive .

## Interactively run the Docker image.
run:
	docker run --rm -it davidtourigny/dfba:latest

## Apply code quality assurance tools.
qa:
	isort --recursive src/dfba tests/ setup.py examples
	black src/dfba tests/ setup.py examples

################################################################################
# Self Documenting Commands                                                    #
################################################################################

.DEFAULT_GOAL := show-help

# Inspired by
# <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
.PHONY: show-help
show-help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && \
		echo '--no-init --raw-control-chars')
