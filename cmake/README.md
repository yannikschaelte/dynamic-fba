## cmake modules

* [`./FindGLPK.cmake`](./FindGLPK.cmake) adapted from [ycollet/coinor-cmake](https://github.com/ycollet/coinor-cmake/blob/master/Clp/cmake/FindGlpk.cmake)
* [`./FindSUNDIALS.cmake`](./FindSUNDIALS.cmake) adapted from [casadi/casadi](https://github.com/casadi/casadi/blob/master/cmake/FindSUNDIALS.cmake)
* [`./README.md`](./README.md): this document

