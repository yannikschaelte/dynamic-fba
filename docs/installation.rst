Installation
============

.. _GLPK: https://www.gnu.org/software/glpk/
.. _SUNDIALS: https://computation.llnl.gov/projects/sundials
.. _Python: https://www.python.org/
.. _cobrapy: https://github.com/opencobra/cobrapy
.. _optlang: https://github.com/biosustain/optlang
.. _symengine: https://github.com/symengine/symengine
.. _pandas: https://pandas.pydata.org
.. _example1: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/examples/example1.py

Currently, we do not provide Python wheels for this package and therefore :ref:`Installing from source` is a bit more involved. The quickest way to run the software
is from the provided `Docker <https://docs.docker.com/>`_ image. There are several ways to do this; we suggest using the following command to run Example 1 from the root of this repsitory:

.. code-block:: console

    docker run -it -v ${PWD}:/opt/examples davidtourigny/dfba python3 examples/example1.py

Provided `Docker <https://docs.docker.com/>`_ is installed correctly, the first time you run this command the latest image will be pulled from dockerhub (unless you have previously pulled or built it yourself using the instructions at the bottom of this page). All subsequent runs will use this same image, and you can replace  ``examples/example1.py`` with the path to a Python_ script containing your own DFBA model. To access results of each simulation, we recommend including the pandas_ method  ``to_csv()`` inside your script in order to write the data frame of results to a csv file (e.g., see :doc:`example1` or uncomment lines 107 and 108 in script example1_).

.. _Installing from source:

Installing from source
----------------------------------------

Currently this package is compatible with most UNIX-like operating systems.
Provided the following :ref:`Dependencies` are installed, the module
can be installed from the root of the `repository <https://gitlab.com/davidtourigny/dynamic-fba>`_ using the command:

.. code-block:: console

    pip install .

.. _Dependencies:

Dependencies
~~~~~~~~~~~~

.. _`build_glpk.sh`: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/scripts/build_glpk.sh
.. _`build_pybind11.sh`: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/scripts/build_pybind11.sh
.. _`build_sundials.sh`: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/scripts/build_sundials.sh
.. _Dockerfile: https://gitlab.com/davidtourigny/dynamic-fba/tree/main/Dockerfile
.. _`pybind11`: https://github.com/pybind/pybind11


* A version of Python_ 3.6 or higher is required
* You need `cmake <https://cmake.org/>`_ for the build process
* You will need `git <https://git-scm.com/>`_ to clone this repository to access
  the scripts and build files
* You need a working compiler with C++11 support, for example, by installing
  ``build-essential`` on Debian-derived Linux systems
* GLPK_ version 4.65 is required or can be installed using `build_glpk.sh`_
* SUNDIALS_ version 5.0.0 or higher is required or can be installed using `build_sundials.sh`_
* pybind11_ is required or can be installed using `build_pybind11.sh`_

Be aware that some of these packages have their own dependencies that must
therefore be installed also (e.g. GLPK_ depends on `GMP <https://gmplib.org/>`_
and pybind11_ requires `pytest <https://docs.pytest.org/en/latest/>`_).


Alternatively, a Dockerfile_ is provided for building a `Docker <https://docs.docker.com/>`_
image to run the software from an interactive container. The `Docker <https://docs.docker.com/>`_ image can be
built in one step by issuing the command:

.. code-block:: console

    make build

from the root of the `repository <https://gitlab.com/davidtourigny/dynamic-fba>`_. It can then be started using:

.. code-block:: console

    make run
