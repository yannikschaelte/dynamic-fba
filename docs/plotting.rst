Visualization
-------------

Visualization tools are available as an extra dependency, optionally installed 
from the root of the `repository <https://gitlab.com/davidtourigny/dynamic-fba>`_ using the commands.

.. code-block:: console

    pip install .[plotly]

or

.. code-block:: console

    pip install .[matplotlib]

Usage of these libraries in the package is exemplified in :doc:`example1`.

It is also possible to run `Jupyterlab <https://jupyterlab.readthedocs.io/en/stable/>`_
in an interactive `Docker <https://docs.docker.com/>`_ container and access the visualization tools
through your browser:

.. code-block:: console

    docker run --rm -it -p 8888:8888 -v $PWD:/opt midnighter/dfba:interactive bash

Once the image is running, start `Jupyterlab <https://jupyterlab.readthedocs.io/en/stable/>`_ using the command:

.. code-block:: console

    jupyter lab --ip 0.0.0.0 --no-browser --allow-root

This will display a URL that should be used to open the notebook in your browser. From
there you will be able to interact with the examples and build your own DFBA model.
